/*
* public_news.cs
*
* 功 能： N/A
* 类 名： public_news
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;

namespace XHD.Model
{
    /// <summary>
    ///     public_news :实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public class public_news
    {
        #region Model

        private int? _create_id;
        private string _create_name;
        private DateTime? _delete_time;
        private int? _dep_id;
        private string _dep_name;
        private int _id;
        private int? _isdelete;
        private string _news_content;
        private DateTime? _news_time;
        private string _news_title;

        /// <summary>
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }

        /// <summary>
        /// </summary>
        public string news_title
        {
            set { _news_title = value; }
            get { return _news_title; }
        }

        /// <summary>
        /// </summary>
        public string news_content
        {
            set { _news_content = value; }
            get { return _news_content; }
        }

        /// <summary>
        /// </summary>
        public int? create_id
        {
            set { _create_id = value; }
            get { return _create_id; }
        }

        /// <summary>
        /// </summary>
        public string create_name
        {
            set { _create_name = value; }
            get { return _create_name; }
        }

        /// <summary>
        /// </summary>
        public int? dep_id
        {
            set { _dep_id = value; }
            get { return _dep_id; }
        }

        /// <summary>
        /// </summary>
        public string dep_name
        {
            set { _dep_name = value; }
            get { return _dep_name; }
        }

        /// <summary>
        /// </summary>
        public DateTime? news_time
        {
            set { _news_time = value; }
            get { return _news_time; }
        }

        /// <summary>
        /// </summary>
        public int? isDelete
        {
            set { _isdelete = value; }
            get { return _isdelete; }
        }

        /// <summary>
        /// </summary>
        public DateTime? Delete_time
        {
            set { _delete_time = value; }
            get { return _delete_time; }
        }

        #endregion Model
    }
}