﻿/*
* CRM_receive.cs
*
* 功 能： N/A
* 类 名： CRM_receive
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 21:12:40    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;

namespace XHD.Model
{
    /// <summary>
    ///     CRM_receive:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public class CRM_receive
    {
        #region Model

        private int? _c_depid;
        private int? _c_empid;
        private DateTime? _create_date;
        private int? _create_id;
        private int? _customer_id;
        private DateTime? _delete_time;
        private int _id;
        private int? _isdelete;
        private int? _order_id;
        private int? _pay_type_id;
        private decimal? _receive_amount;
        private DateTime? _receive_date;
        private int? _receive_direction_id;
        private string _receive_num;
        private decimal? _receive_real;
        private string _remarks;

        /// <summary>
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }

        /// <summary>
        /// </summary>
        public int? Customer_id
        {
            set { _customer_id = value; }
            get { return _customer_id; }
        }

        /// <summary>
        /// </summary>
        public string Receive_num
        {
            set { _receive_num = value; }
            get { return _receive_num; }
        }

        /// <summary>
        /// </summary>
        public int? Pay_type_id
        {
            set { _pay_type_id = value; }
            get { return _pay_type_id; }
        }

        /// <summary>
        /// </summary>
        public decimal? Receive_amount
        {
            set { _receive_amount = value; }
            get { return _receive_amount; }
        }

        /// <summary>
        /// </summary>
        public DateTime? Receive_date
        {
            set { _receive_date = value; }
            get { return _receive_date; }
        }

        /// <summary>
        /// </summary>
        public int? C_depid
        {
            set { _c_depid = value; }
            get { return _c_depid; }
        }

        /// <summary>
        /// </summary>
        public int? C_empid
        {
            set { _c_empid = value; }
            get { return _c_empid; }
        }

        /// <summary>
        /// </summary>
        public int? create_id
        {
            set { _create_id = value; }
            get { return _create_id; }
        }

        /// <summary>
        /// </summary>
        public DateTime? create_date
        {
            set { _create_date = value; }
            get { return _create_date; }
        }

        /// <summary>
        /// </summary>
        public int? order_id
        {
            set { _order_id = value; }
            get { return _order_id; }
        }

        /// <summary>
        /// </summary>
        public string remarks
        {
            set { _remarks = value; }
            get { return _remarks; }
        }

        /// <summary>
        /// </summary>
        public int? isDelete
        {
            set { _isdelete = value; }
            get { return _isdelete; }
        }

        /// <summary>
        /// </summary>
        public DateTime? Delete_time
        {
            set { _delete_time = value; }
            get { return _delete_time; }
        }

        /// <summary>
        /// </summary>
        public int? receive_direction_id
        {
            set { _receive_direction_id = value; }
            get { return _receive_direction_id; }
        }

        /// <summary>
        /// </summary>
        public decimal? receive_real
        {
            set { _receive_real = value; }
            get { return _receive_real; }
        }

        #endregion Model
    }
}