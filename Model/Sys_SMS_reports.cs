﻿/*
* Sys_SMS_reports.cs
*
* 功 能： N/A
* 类 名： Sys_SMS_reports
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015/8/24 11:52:37    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com  www.xhdoa.com. All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：小黄豆                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace XHD.Model
{
	/// <summary>
	/// Sys_SMS_reports:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Sys_SMS_reports
	{
		public Sys_SMS_reports()
		{}
		#region Model
		private int _id;
		private int _reportstatus;
		private string _mobile;
		private string _submitdate;
		private string _receivedate;
		private string _errorcode;
		private string _memo;
		private string _servicecodeadd;
		private long? _seqid;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int reportStatus
		{
			set{ _reportstatus=value;}
			get{return _reportstatus;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mobile
		{
			set{ _mobile=value;}
			get{return _mobile;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string submitDate
		{
			set{ _submitdate=value;}
			get{return _submitdate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string receiveDate
		{
			set{ _receivedate=value;}
			get{return _receivedate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string errorCode
		{
			set{ _errorcode=value;}
			get{return _errorcode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string memo
		{
			set{ _memo=value;}
			get{return _memo;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string serviceCodeAdd
		{
			set{ _servicecodeadd=value;}
			get{return _servicecodeadd;}
		}
		/// <summary>
		/// 
		/// </summary>
		public long? seqID
		{
			set{ _seqid=value;}
			get{return _seqid;}
		}
		#endregion Model

	}
}

