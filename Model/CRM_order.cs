﻿/*
* CRM_order.cs
*
* 功 能： N/A
* 类 名： CRM_order
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 19:49:34    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;

namespace XHD.Model
{
    /// <summary>
    ///     CRM_order:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public class CRM_order
    {
        #region Model

        private decimal? _arrears_invoice;
        private decimal? _arrears_money;
        private int? _c_dep_id;
        private int? _c_emp_id;
        private DateTime? _create_date;
        private int? _create_id;
        private int? _customer_id;
        private DateTime? _delete_time;
        private int? _f_dep_id;
        private int? _f_emp_id;
        private int _id;
        private decimal? _invoice_money;
        private int? _isdelete;
        private decimal? _order_amount;
        private DateTime? _order_date;
        private string _order_details;
        private int? _order_status_id;
        private int? _pay_type_id;
        private decimal? _receive_money;
        private string _serialnumber;

        /// <summary>
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }

        /// <summary>
        /// </summary>
        public string Serialnumber
        {
            set { _serialnumber = value; }
            get { return _serialnumber; }
        }

        /// <summary>
        /// </summary>
        public int? Customer_id
        {
            set { _customer_id = value; }
            get { return _customer_id; }
        }

        /// <summary>
        /// </summary>
        public DateTime? Order_date
        {
            set { _order_date = value; }
            get { return _order_date; }
        }

        /// <summary>
        /// </summary>
        public int? pay_type_id
        {
            set { _pay_type_id = value; }
            get { return _pay_type_id; }
        }

        /// <summary>
        /// </summary>
        public string Order_details
        {
            set { _order_details = value; }
            get { return _order_details; }
        }

        /// <summary>
        /// </summary>
        public int? Order_status_id
        {
            set { _order_status_id = value; }
            get { return _order_status_id; }
        }

        /// <summary>
        /// </summary>
        public decimal? Order_amount
        {
            set { _order_amount = value; }
            get { return _order_amount; }
        }

        /// <summary>
        /// </summary>
        public int? create_id
        {
            set { _create_id = value; }
            get { return _create_id; }
        }

        /// <summary>
        /// </summary>
        public DateTime? create_date
        {
            set { _create_date = value; }
            get { return _create_date; }
        }

        /// <summary>
        /// </summary>
        public int? C_dep_id
        {
            set { _c_dep_id = value; }
            get { return _c_dep_id; }
        }

        /// <summary>
        /// </summary>
        public int? C_emp_id
        {
            set { _c_emp_id = value; }
            get { return _c_emp_id; }
        }

        /// <summary>
        /// </summary>
        public int? F_dep_id
        {
            set { _f_dep_id = value; }
            get { return _f_dep_id; }
        }

        /// <summary>
        /// </summary>
        public int? F_emp_id
        {
            set { _f_emp_id = value; }
            get { return _f_emp_id; }
        }

        /// <summary>
        /// </summary>
        public decimal? receive_money
        {
            set { _receive_money = value; }
            get { return _receive_money; }
        }

        /// <summary>
        /// </summary>
        public decimal? arrears_money
        {
            set { _arrears_money = value; }
            get { return _arrears_money; }
        }

        /// <summary>
        /// </summary>
        public decimal? invoice_money
        {
            set { _invoice_money = value; }
            get { return _invoice_money; }
        }

        /// <summary>
        /// </summary>
        public decimal? arrears_invoice
        {
            set { _arrears_invoice = value; }
            get { return _arrears_invoice; }
        }

        /// <summary>
        /// </summary>
        public int? isDelete
        {
            set { _isdelete = value; }
            get { return _isdelete; }
        }

        /// <summary>
        /// </summary>
        public DateTime? Delete_time
        {
            set { _delete_time = value; }
            get { return _delete_time; }
        }

        #endregion Model
    }
}