﻿/*
* hr_employee.cs
*
* 功 能： N/A
* 类 名： hr_employee
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System.Data;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using XHD.Common;
using XHD.Controller;

namespace XHD.Server
{
    public class hr_employee
    {
        public static BLL.hr_employee emp = new BLL.hr_employee();
        public static Model.hr_employee model = new Model.hr_employee();

        public HttpContext Context;
        public int emp_id;
        public string emp_name;
        public Model.hr_employee employee;
        public HttpRequest request;
        public string uid;


        public hr_employee()
        {
        }

        public hr_employee(HttpContext context)
        {
            Context = context;
            request = context.Request;

            var userinfo = new User_info();
            employee = userinfo.GetCurrentEmpInfo(context);

            emp_id = employee.ID;
            emp_name = PageValidate.InputText(employee.name, 50);
            uid = PageValidate.InputText(employee.uid, 50);
        }

        public string grid()
        {
            int PageIndex = int.Parse(request["page"] == null ? "1" : request["page"]);
            int PageSize = int.Parse(request["pagesize"] == null ? "30" : request["pagesize"]);
            string sortname = request["sortname"];
            string sortorder = request["sortorder"];

            if (string.IsNullOrEmpty(sortname))
                sortname = " ID";
            if (string.IsNullOrEmpty(sortorder))
                sortorder = " desc";

            string sorttext = " " + sortname + " " + sortorder;

            string Total;
            string serchtxt = "uid != 'admin'";

            string did = request["did"];
            if (PageValidate.IsNumber(did))
                serchtxt += " and d_id=" + int.Parse(did);

            //string authtxt = request["auth"];
            //if (authtxt == "1")
            //{
            //    var dataauth = new GetDataAuth();
            //    string txt = dataauth.GetDataAuthByid("1", "Sys_view", emp_id);
            //    string[] arr = txt.Split(':');
            //    switch (arr[0])
            //    {
            //        case "my":
            //            serchtxt += " and ID=" + emp_id;
            //            break;
            //    }
            //}

            if (!string.IsNullOrEmpty(request["stext"]))
            {
                if (request["stext"] != "输入姓名搜索")
                    serchtxt += " and name like N'%" + PageValidate.InputText(request["stext"], 255) + "%'";
            }
            //权限
            DataSet ds = emp.GetList(PageSize, PageIndex, serchtxt, sorttext, out Total);

            string dt = GetGridJSON.DataTableToJSON1(ds.Tables[0], Total);
            return dt;
        }

        //表格json
        public string getRole(int empid)
        {
            DataSet ds = emp.GetRole(empid);

            string dt = GetGridJSON.DataTableToJSON(ds.Tables[0]);

            return dt;
        }

        //validate
        public string Exist()
        {
            string user_id = request["T_uid"];
            string T_emp_id = request["emp_id"];
            if (string.IsNullOrEmpty(T_emp_id) || T_emp_id == "null")
                T_emp_id = "0";

            DataSet ds1 =
                emp.GetList(" uid='" + PageValidate.InputText(user_id, 250) + "' and  ID!=" + int.Parse(T_emp_id));

            return (ds1.Tables[0].Rows.Count > 0 ? "false" : "true");
        }

        //Form JSON
        public string form()
        {
            string id = PageValidate.InputText(request["id"], 50);
            if (id == "epu")
                id = emp_id.ToString();

            DataSet ds = emp.GetList("id=" + id);

            string dt = DataToJson.DataToJSON(ds);

            return dt;
        }

        //save
        public void save()
        {
            model.uid = PageValidate.InputText(request["T_uid"], 255);
            model.email = PageValidate.InputText(request["T_email"], 255);
            model.name = PageValidate.InputText(request["T_name"], 255);
            model.birthday = PageValidate.InputText(request["T_birthday"], 255);
            model.sex = PageValidate.InputText(request["T_sex"], 255);
            model.idcard = PageValidate.InputText(request["T_idcard"], 255);
            model.tel = PageValidate.InputText(request["T_tel"], 255);
            model.status = PageValidate.InputText(request["T_status"], 255);
            model.EntryDate = PageValidate.InputText(request["T_entryDate"], 255);
            model.address = PageValidate.InputText(request["T_Adress"], 255);
            model.schools = PageValidate.InputText(request["T_school"], 255);
            model.education = PageValidate.InputText(request["T_edu"], 255);
            model.professional = PageValidate.InputText(request["T_professional"], 255);
            model.remarks = PageValidate.InputText(request["T_remarks"], 255);
            model.title = PageValidate.InputText(request["headurl"], 255);
            model.canlogin = int.Parse(request["canlogin"]);

            int empid;
            string id = PageValidate.InputText(request["id"], 50);
            if (PageValidate.IsNumber(id))
            {
                DataSet ds = emp.GetList(" ID=" + int.Parse(id));
                DataRow dr = ds.Tables[0].Rows[0];
                model.ID = int.Parse(id);
                empid = model.ID;

                emp.Update(model);

                var log = new sys_log();

                int UserID = emp_id;
                string UserName = emp_name;
                string IPStreet = request.UserHostAddress;
                string EventTitle = model.name;
                string EventType = "员工修改";
                int EventID = model.ID;
                string Log_Content = null;

                if (dr["email"].ToString() != request["T_email"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "邮箱", dr["email"], request["T_email"]);

                if (dr["name"].ToString() != request["T_name"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "员工姓名", dr["name"], request["T_name"]);

                if (dr["birthday"].ToString() != request["T_birthday"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "员工生日", dr["birthday"], request["T_birthday"]);

                if (dr["sex"].ToString() != request["T_sex"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "员工性别", dr["sex"], request["T_sex"]);

                if (dr["status"].ToString() != request["T_status"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "状态", dr["status"], request["T_status"]);

                if (dr["idcard"].ToString() != request["T_idcard"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "身份证", dr["idcard"], request["T_idcard"]);

                if (dr["tel"].ToString() != request["T_tel"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "手机", dr["tel"], request["T_tel"]);

                if (dr["EntryDate"].ToString() != request["T_entryDate"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "入职日期", dr["EntryDate"], request["T_entryDate"]);

                if (dr["address"].ToString() != request["T_Adress"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "地址", dr["address"], request["T_Adress"]);

                if (dr["schools"].ToString() != request["T_school"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "毕业学校", dr["schools"], request["T_school"]);

                if (dr["education"].ToString() != request["T_edu"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "学历", dr["education"], request["T_edu"]);

                if (dr["professional"].ToString() != request["T_professional"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "专业", dr["professional"],
                        request["T_professional"]);

                if (dr["remarks"].ToString() != request["T_remarks"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "备注", dr["remarks"], request["T_remarks"]);

                if (dr["canlogin"].ToString() != request["canlogin"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "能否登录", dr["canlogin"], request["canlogin"]);

                if (!string.IsNullOrEmpty(Log_Content))
                    log.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID, Log_Content);
            }
            else
            {
                model.isDelete = 0;
                model.pwd = FormsAuthentication.HashPasswordForStoringInConfigFile("123456", "MD5");
                empid = emp.Add(model);
            }

            //post
            string json = request["PostData"].ToLower();
            var js = new JavaScriptSerializer();

            PostData[] postdata;
            postdata = js.Deserialize<PostData[]>(json);

            var hp = new BLL.hr_post();
            var modelpost = new Model.hr_post();

            modelpost.emp_id = empid;
            model.ID = empid;

            for (int i = 0; i < postdata.Length; i++)
            {
                modelpost.id = postdata[i].Post_id;
                modelpost.default_post = postdata[i].Default_post;

                if (postdata[i].Default_post == 1)
                {
                    model.d_id = postdata[i].dep_id;
                    model.position_id = postdata[i].Position_id;
                    model.postid = postdata[i].Post_id;
                    //context.Response.Write(postdata[i].Depname + "@");
                    //更新默认岗位
                    emp.UpdatePost(model);

                    // 更新客户，订单，合同，收款，开票 人员
                    //emp.UpdateCOCRI(model);

                    //清除员工
                    hp.UpdatePostEmpbyEid(empid);
                }

                //设置员工
                hp.UpdatePostEmp(modelpost);
                //context.Response.Write("{success:success}");
            }
        }
        public void updateDefaultCity(string city)
        {
            model.default_city = PageValidate.InputText(city, 50);
            model.ID = emp_id;
            emp.UpdateDefaultCity(model);
        }
        public string getDefaultCity()
        {
            return employee.default_city;
        }
        public string PersonalUpdate()
        {
            model.email = PageValidate.InputText(request["T_email"], 255);
            model.name = PageValidate.InputText(request["T_name"], 255);
            model.birthday = PageValidate.InputText(request["T_birthday"], 255);
            model.sex = PageValidate.InputText(request["T_sex"], 255);
            model.idcard = PageValidate.InputText(request["T_idcard"], 255);
            model.tel = PageValidate.InputText(request["T_tel"], 255);


            model.address = PageValidate.InputText(request["T_Adress"], 255);
            model.schools = PageValidate.InputText(request["T_school"], 255);
            model.education = PageValidate.InputText(request["T_edu"], 255);
            model.professional = PageValidate.InputText(request["T_professional"], 255);
            model.remarks = PageValidate.InputText(request["T_remarks"], 255);
            model.title = PageValidate.InputText(request["headurl"], 255);

            DataSet dsemp = emp.GetList(string.Format("ID = {0}", emp_id));
            DataRow dr = dsemp.Tables[0].Rows[0];
            model.ID = emp_id;

            bool isup = emp.PersonalUpdate(model);

            if (isup)
            {
                var log = new sys_log();

                int UserID = emp_id;
                string UserName = emp_name;
                string IPStreet = request.UserHostAddress;
                string EventTitle = model.name;
                string EventType = "个人信息修改";
                int EventID = emp_id;
                string Log_Content = null;

                if (dr["email"].ToString() != request["T_email"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "邮箱", dr["email"], request["T_email"]);

                if (dr["name"].ToString() != request["T_name"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "员工姓名", dr["name"], request["T_name"]);

                if (dr["birthday"].ToString() != request["T_birthday"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "员工生日", dr["birthday"], request["T_birthday"]);

                if (dr["sex"].ToString() != request["T_sex"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "员工性别", dr["sex"], request["T_sex"]);

                if (dr["idcard"].ToString() != request["T_idcard"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "身份证", dr["idcard"], request["T_idcard"]);

                if (dr["tel"].ToString() != request["T_tel"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "手机", dr["tel"], request["T_tel"]);

                if (dr["address"].ToString() != request["T_Adress"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "地址", dr["address"], request["T_Adress"]);

                if (dr["schools"].ToString() != request["T_school"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "毕业学校", dr["schools"], request["T_school"]);

                if (dr["education"].ToString() != request["T_edu"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "学历", dr["education"], request["T_edu"]);

                if (dr["professional"].ToString() != request["T_professional"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "专业", dr["professional"],
                        request["T_professional"]);

                log.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID, Log_Content);

                return "true";
            }
            else
                return "false";
        }

        //changepwd
        public string changepwd()
        {
            DataSet ds = emp.GetPWD(emp_id);

            string oldpwd = FormsAuthentication.HashPasswordForStoringInConfigFile(request["T_oldpwd"], "MD5");
            string newpwd = FormsAuthentication.HashPasswordForStoringInConfigFile(request["T_newpwd"], "MD5");

            if (ds.Tables[0].Rows[0]["pwd"].ToString() == oldpwd)
            {
                model.pwd = newpwd;
                model.ID = (emp_id);
                emp.changepwd(model);
                return ("true");
            }
            return ("false");
        }

        //allchangepwd
        public void allchangepwd()
        {
            string empid = request["empid"];

            string newpwd = FormsAuthentication.HashPasswordForStoringInConfigFile(request["T_newpwd"], "MD5");

            model.pwd = newpwd;
            model.ID = int.Parse(empid);
            emp.changepwd(model);
        }

        //del
        public string del(int id)
        {
            var hp = new BLL.hr_post();

            string EventType = "员工删除";

            DataSet ds = emp.GetList(" id=" + id);
            var customer = new BLL.CRM_Customer();
            int cc = customer.GetList("Employee_id=" + id).Tables[0].Rows.Count;

            if (cc > 0)
            {
                return ("false:customer");
            }
            bool isdel = false;
            isdel = emp.Delete(id);
            //update post
            hp.UpdatePostEmpbyEid(id);


            if (isdel)
            {
                int UserID = emp_id;
                string UserName = emp_name;
                string IPStreet = request.UserHostAddress;
                int EventID = id;
                string EventTitle = ds.Tables[0].Rows[0]["name"].ToString();

                var log = new sys_log();
                log.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID, null);

                return ("true");
            }
            return ("false");
        }

        public class PostData
        {
            public int? dep_id { get; set; }
            public int Post_id { get; set; }
            public string Post_name { get; set; }
            public int? Emp_id { get; set; }
            public string Emp_name { get; set; }
            public int? Default_post { get; set; }
            public string Depname { get; set; }
            public int? Position_id { get; set; }
            public string Position_name { get; set; }
        }
    }
}