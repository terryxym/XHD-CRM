﻿/*
* upload.cs
*
* 功 能： N/A
* 类 名： upload
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.UI;
using XHD.Common;
using XHD.Controller;

namespace XHD.Server
{
    public class upload
    {
        public HttpContext Context;
        public HttpRequest request;

        public upload()
        {
        }

        public upload(HttpContext context)
        {
            Context = context;
            request = context.Request;
        }

        public string upfiles(string ftype)
        {
            string success = "";
            HttpPostedFile uploadFile = request.Files[0];
            string fileName = uploadFile.FileName;
            //string sExt = fileName.Substring(fileName.LastIndexOf(".")).ToLower();

            switch (ftype)
            {
                case "tmp_headimg":success = headimg(uploadFile);
                    break;
                case "cus_import":success = cus_import(uploadFile);
                    break;
                case "contact_import": success = contact_import(uploadFile);
                    break;
                case "contract": success = contract(uploadFile);
                    break;
                default:
                    success = "";
                    break;
            }

            return success;
        }

        public string headimg(HttpPostedFile uploadFile)
        {
            string fileName = uploadFile.FileName;
            uploadFile.SaveAs(Context.Server.MapPath(@"~/images/upload/temp/" + fileName));
            return (@"../images/upload/temp/" + fileName);
        }

        public string cus_import(HttpPostedFile uploadFile)
        {
            string fileName = uploadFile.FileName;

            string nowfileName = "Customer.xls";

            uploadFile.SaveAs(Context.Server.MapPath(@"~/file/customer/" + nowfileName));

            return (nowfileName);
        }
        public string contact_import(HttpPostedFile uploadFile)
        {
            string fileName = uploadFile.FileName;

            string nowfileName = "contact.xls" ;

            uploadFile.SaveAs(Context.Server.MapPath(@"~/file/contact/" + nowfileName));

            return (nowfileName);
        }
        public string contract(HttpPostedFile uploadFile)
        {

            string filename = uploadFile.FileName;
            string sExt = filename.Substring(filename.LastIndexOf(".")).ToLower();
            DateTime now = DateTime.Now;
            string nowfileName = now.ToString("yyyyMMddHHmmss") + Assistant.GetRandomNum(6) + sExt;

            uploadFile.SaveAs(HttpContext.Current.Server.MapPath(@"~/file/contract/" + nowfileName));
            return nowfileName;
        }

        public string upheadimg()
        {
            int x1 = int.Parse(request["x1"]);
            int y1 = int.Parse(request["y1"]);
            int w = int.Parse(request["w"]);
            int h = int.Parse(request["h"]);

            string fileName = request["txtFileName"];
            fileName = fileName.Substring(fileName.LastIndexOf('\\') + 1);
            string sExt = fileName.Substring(fileName.LastIndexOf(".")).ToLower();

            DateTime now = DateTime.Now;
            string nowfileName = now.ToString("yyyyMMddHHmmss") + Assistant.GetRandomNum(6) + sExt;

            var page = new Page();

            string oldpath = page.Server.MapPath(@"~/images/upload/temp/" + fileName);
            string currpath = page.Server.MapPath(@"~/images/upload/portrait/" + nowfileName);

            Image originalImg = Image.FromFile(oldpath);

            ZoomImage.SaveCutPic(oldpath, currpath, 0, 0, w, h, x1, y1,
                Convert.ToInt32(300 * originalImg.Width / originalImg.Height), 300);

            originalImg.Dispose();

            File.Delete(oldpath);

            return (nowfileName);
        }


    }
}