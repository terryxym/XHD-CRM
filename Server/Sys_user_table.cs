﻿/*
* Sys_user_table.cs
*
* 功 能： N/A
* 类 名： Sys_user_table
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using XHD.Common;
using XHD.Controller;

namespace XHD.Server
{
    public class Sys_user_table
    {
         public static BLL.Sys_user_table tab = new BLL.Sys_user_table();
        public static Model.Sys_user_table model = new Model.Sys_user_table();

        public HttpContext Context;
        public int emp_id;
        public string emp_name;
        public Model.hr_employee employee;
        public HttpRequest request;
        public string uid;


        public Sys_user_table()
        {
        }

        public Sys_user_table(HttpContext context)
        {
            Context = context;
            request = context.Request;

            var userinfo = new User_info();
            employee = userinfo.GetCurrentEmpInfo(context);

            emp_id = employee.ID;
            emp_name = PageValidate.InputText(employee.name, 50);
            uid = PageValidate.InputText(employee.uid, 50);
        }

        public string getColumn(int MenuID)
        {
            model.emp_id = emp_id;
            model.menu_id = MenuID;
            DataSet ds = tab.GetList(model);
            string dt = GetGridJSON.DataTableToJSON(ds.Tables[0]);
            return dt;
        }

        public string setColumn(string type,int MenuID,int columnID,int value)
        {
            if(string.IsNullOrEmpty(type)) return "No type";
            model.emp_id = emp_id;
            model.column_id = columnID;
            model.menu_id = MenuID;
            switch (type)
            {
                case "width":
                    model.column_width = value;
                    break;
                case "column":
                    model.isHide = value;
                    break;
            }

            bool be = tab.Exists(model);

            if (be)
            {
                switch (type)
                {
                    case "width":
                        tab.UpdateWidth(model);
                        break;
                    case "column":
                        tab.UpdateHide(model);
                        break;
                }
                return "update sucess";
            }
            else
            {
                tab.Add(model);
                return "add sucess";
            }
           
        }
    }
}
