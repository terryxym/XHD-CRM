﻿/*
* CRM_product.cs
*
* 功 能： N/A
* 类 名： CRM_product
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-24 10:03:12    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using XHD.DBUtility;

//Please add references

namespace XHD.DAL
{
    /// <summary>
    ///     数据访问类:CRM_product
    /// </summary>
    public class CRM_product
    {
        #region  BasicMethod

        /// <summary>
        ///     增加一条数据
        /// </summary>
        public int Add(Model.CRM_product model)
        {
            var strSql = new StringBuilder();
            strSql.Append("insert into CRM_product(");
            strSql.Append("product_name,category_id,specifications,status,unit,remarks,price,isDelete,Delete_time)");
            strSql.Append(" values (");
            strSql.Append(
                "@product_name,@category_id,@specifications,@status,@unit,@remarks,@price,@isDelete,@Delete_time)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters =
            {
                new SqlParameter("@product_name", SqlDbType.VarChar, 250),
                new SqlParameter("@category_id", SqlDbType.Int, 4),
                new SqlParameter("@specifications", SqlDbType.VarChar, 250),
                new SqlParameter("@status", SqlDbType.VarChar, 250),
                new SqlParameter("@unit", SqlDbType.VarChar, 250),
                new SqlParameter("@remarks", SqlDbType.VarChar, -1),
                new SqlParameter("@price", SqlDbType.Float, 8),
                new SqlParameter("@isDelete", SqlDbType.Int, 4),
                new SqlParameter("@Delete_time", SqlDbType.DateTime)
            };
            parameters[0].Value = model.product_name;
            parameters[1].Value = model.category_id;
            parameters[2].Value = model.specifications;
            parameters[3].Value = model.status;
            parameters[4].Value = model.unit;
            parameters[5].Value = model.remarks;
            parameters[6].Value = model.price;
            parameters[7].Value = model.isDelete;
            parameters[8].Value = model.Delete_time;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            return Convert.ToInt32(obj);
        }

        /// <summary>
        ///     更新一条数据
        /// </summary>
        public bool Update(Model.CRM_product model)
        {
            var strSql = new StringBuilder();
            strSql.Append("update CRM_product set ");
            strSql.Append("product_name=@product_name,");
            strSql.Append("category_id=@category_id,");
            strSql.Append("specifications=@specifications,");
            strSql.Append("status=@status,");
            strSql.Append("unit=@unit,");
            strSql.Append("remarks=@remarks,");
            strSql.Append("price=@price");
            strSql.Append(" where product_id=@product_id");
            SqlParameter[] parameters =
            {
                new SqlParameter("@product_name", SqlDbType.VarChar, 250),
                new SqlParameter("@category_id", SqlDbType.Int, 4),
                new SqlParameter("@specifications", SqlDbType.VarChar, 250),
                new SqlParameter("@status", SqlDbType.VarChar, 250),
                new SqlParameter("@unit", SqlDbType.VarChar, 250),
                new SqlParameter("@remarks", SqlDbType.VarChar, -1),
                new SqlParameter("@price", SqlDbType.Float, 8),
                new SqlParameter("@product_id", SqlDbType.Int, 4)
            };
            parameters[0].Value = model.product_name;
            parameters[1].Value = model.category_id;
            parameters[2].Value = model.specifications;
            parameters[3].Value = model.status;
            parameters[4].Value = model.unit;
            parameters[5].Value = model.remarks;
            parameters[6].Value = model.price;
            parameters[7].Value = model.product_id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool Delete(int product_id)
        {
            var strSql = new StringBuilder();
            strSql.Append("delete from CRM_product ");
            strSql.Append(" where product_id=@product_id");
            SqlParameter[] parameters =
            {
                new SqlParameter("@product_id", SqlDbType.Int, 4)
            };
            parameters[0].Value = product_id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     批量删除数据
        /// </summary>
        public bool DeleteList(string product_idlist)
        {
            var strSql = new StringBuilder();
            strSql.Append("delete from CRM_product ");
            strSql.Append(" where product_id in (" + product_idlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            var strSql = new StringBuilder();
            strSql.Append(
                "select product_id,product_name,category_id,specifications,status,unit,remarks,price,isDelete,Delete_time ");
            strSql.Append(
                ",(select product_category from CRM_product_category where id = CRM_product.[category_id]) as [category_name] ");
            strSql.Append(" FROM CRM_product ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            var strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top);
            }
            strSql.Append(
                " product_id,product_name,category_id,specifications,status,unit,remarks,price,isDelete,Delete_time ");
            strSql.Append(
                ",(select product_category from CRM_product_category where id = CRM_product.[category_id]) as [category_name] ");
            strSql.Append(" FROM CRM_product ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            var strSql_grid = new StringBuilder();
            var strSql_total = new StringBuilder();
            strSql_total.Append(" SELECT COUNT(product_id) FROM CRM_product ");
            strSql_grid.Append("SELECT ");
            strSql_grid.Append(
                "      product_id,product_name,category_id,specifications,status,unit,remarks,price,isDelete,Delete_time ");
            strSql_grid.Append(
                ",(select product_category from CRM_product_category where id = w1.[category_id]) as [category_name] ");
            strSql_grid.Append(
                " FROM ( SELECT product_id,product_name,category_id,specifications,status,unit,remarks,price,isDelete,Delete_time, ROW_NUMBER() OVER( Order by " +
                filedOrder + " ) AS n from CRM_product");
            if (strWhere.Trim() != "")
            {
                strSql_grid.Append(" WHERE " + strWhere);
                strSql_total.Append(" WHERE " + strWhere);
            }
            strSql_grid.Append("  ) as w1  ");
            strSql_grid.Append("WHERE n BETWEEN " + PageSize*(PageIndex - 1) + " AND " + PageSize*PageIndex);
            strSql_grid.Append(" ORDER BY " + filedOrder);
            Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
            return DbHelperSQL.Query(strSql_grid.ToString());
        }

        #endregion  BasicMethod

        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}